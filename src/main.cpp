#include <mbed.h>

DigitalOut myLeds [4] = {LED1, LED2, LED3, LED4}; //Array mit den auf die Platine aufgelöteten LEDs
DigitalIn myButton (p8); //Ein an Pin 8 angeschlossener Knopf
int main() {
  myButton.mode(PullUp);

    while(1) {

        int i = 0;
        for(i = 0; i < 4; i++){


            wait(0.1);
            myLeds[i] = 1;
            int a = (i==0)? 3 : i-1;

            myLeds[a] = 0;

            while (!myButton) { // solange der Button gedrückt ist, wird das Programm "pausiert"

            }
        }
    }
}
